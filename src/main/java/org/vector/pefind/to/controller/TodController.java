package org.vector.pefind.to.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("tod")
public class TodController {

	@RequestMapping("test")
	public String test(){
		System.out.println("TodController.test()");
		return "aaa";
	}
}
