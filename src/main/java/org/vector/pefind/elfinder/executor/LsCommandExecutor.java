package org.vector.pefind.elfinder.executor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;
import org.vector.pefind.elfinder.iexecutor.CommandExecutor;
import org.vector.pefind.elfinder.iservice.FsService;
import org.vector.pefind.elfinder.model.FsItemEx;


public class LsCommandExecutor extends AbstractJsonCommandExecutor implements
		CommandExecutor {
	
	public void execute(FsService fsService, HttpServletRequest request,
			ServletContext servletContext, JSONObject json) throws Exception {
		String target = request.getParameter("target");
		String[] onlyMimes = request.getParameterValues("mimes[]");

		Map<String, FsItemEx> files = new HashMap<String, FsItemEx>();
		FsItemEx fsi = super.findItem(fsService, target);
		super.addChildren(files, fsi, onlyMimes);

		json.put("list", files2JsonArray(request, files.values()));
	}
}
