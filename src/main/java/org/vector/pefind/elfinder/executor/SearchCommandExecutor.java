package org.vector.pefind.elfinder.executor;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.vector.pefind.elfinder.iexecutor.CommandExecutor;
import org.vector.pefind.elfinder.iservice.FsService;
import org.vector.pefind.elfinder.util.FsItemFilterUtils;
import org.json.JSONObject;

public class SearchCommandExecutor extends AbstractJsonCommandExecutor
		implements CommandExecutor {
	
	public void execute(FsService fsService, HttpServletRequest request,
			ServletContext servletContext, JSONObject json) throws Exception {
		json.put(
				"files",
				files2JsonArray(request, FsItemFilterUtils.filterFiles(
						fsService.find(FsItemFilterUtils
								.createFileNameKeywordFilter(request
										.getParameter("q"))), super
								.getRequestedFilter(request))));
	}
}
