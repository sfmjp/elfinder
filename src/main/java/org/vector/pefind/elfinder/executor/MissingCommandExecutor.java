package org.vector.pefind.elfinder.executor;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.vector.pefind.elfinder.exception.ErrorException;
import org.vector.pefind.elfinder.iexecutor.CommandExecutor;
import org.vector.pefind.elfinder.iservice.FsService;
import org.json.JSONObject;

/**
 * This is a command that should be executed when a matching command can't be
 * found.
 */
public class MissingCommandExecutor extends AbstractJsonCommandExecutor
		implements CommandExecutor {
	
	protected void execute(FsService fsService, HttpServletRequest request,
			ServletContext servletContext, JSONObject json) throws Exception {
		String cmd = request.getParameter("cmd");
		throw new ErrorException("errUnknownCmd", cmd);
	}
}
