package org.vector.pefind.elfinder.executor;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.vector.pefind.elfinder.iexecutor.CommandExecutor;
import org.vector.pefind.elfinder.iservice.FsService;
import org.vector.pefind.elfinder.model.FsItemEx;
import org.json.JSONObject;

/**
 * This calculates the total size of all the supplied targets and returns the
 * size in bytes.
 */
public class SizeCommandExecutor extends AbstractJsonCommandExecutor implements
		CommandExecutor {
	
	protected void execute(FsService fsService, HttpServletRequest request,
			ServletContext servletContext, JSONObject json) throws Exception {
		String[] targets = request.getParameterValues("targets[]");
		long size = 0;
		for (String target : targets) {
			FsItemEx item = findItem(fsService, target);
			size += item.getSize();
		}
		json.put("size", size);
	}
}
