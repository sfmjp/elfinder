package org.vector.pefind.elfinder.executor;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.vector.pefind.elfinder.iexecutor.CommandExecutor;
import org.vector.pefind.elfinder.iservice.FsService;
import org.vector.pefind.elfinder.model.FsItemEx;
import org.json.JSONObject;

public class RmCommandExecutor extends AbstractJsonCommandExecutor implements
		CommandExecutor {
	
	public void execute(FsService fsService, HttpServletRequest request,
			ServletContext servletContext, JSONObject json) throws Exception {
		String[] targets = request.getParameterValues("targets[]");
		// String current = request.getParameter("current");
		List<String> removed = new ArrayList<String>();

		for (String target : targets) {
			FsItemEx ftgt = super.findItem(fsService, target);
			ftgt.delete();
			removed.add(ftgt.getHash());
		}

		json.put("removed", removed.toArray());
	}
}
