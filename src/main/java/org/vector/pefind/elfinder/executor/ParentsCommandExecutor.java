package org.vector.pefind.elfinder.executor;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.vector.pefind.elfinder.exception.ErrorException;
import org.vector.pefind.elfinder.iexecutor.CommandExecutor;
import org.vector.pefind.elfinder.iservice.FsService;
import org.vector.pefind.elfinder.model.FsItemEx;
import org.json.JSONObject;

public class ParentsCommandExecutor extends AbstractJsonCommandExecutor
		implements CommandExecutor {
	// This is a limit on the number of parents so that a badly implemented
	// FsService can't
	// result in a runaway thread.
	final static int LIMIT = 1024;

	
	public void execute(FsService fsService, HttpServletRequest request,
			ServletContext servletContext, JSONObject json) throws Exception {
		String target = request.getParameter("target");

		Map<String, FsItemEx> files = new HashMap<String, FsItemEx>();
		FsItemEx fsi = findItem(fsService, target);
		for (int i = 0; !fsi.isRoot(); i++) {
			super.addSubfolders(files, fsi);
			fsi = fsi.getParent();
			if (i > LIMIT) {
				throw new ErrorException(
						"Reached recursion limit on parents of: " + LIMIT);
			}
		}

		json.put("tree", files2JsonArray(request, files.values()));
	}
}
