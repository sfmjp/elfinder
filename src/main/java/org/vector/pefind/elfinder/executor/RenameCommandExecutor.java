package org.vector.pefind.elfinder.executor;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.vector.pefind.elfinder.iexecutor.CommandExecutor;
import org.vector.pefind.elfinder.iservice.FsService;
import org.vector.pefind.elfinder.model.FsItemEx;
import org.json.JSONObject;

public class RenameCommandExecutor extends AbstractJsonCommandExecutor
		implements CommandExecutor {
	
	public void execute(FsService fsService, HttpServletRequest request,
			ServletContext servletContext, JSONObject json) throws Exception {
		String target = request.getParameter("target");
		// String current = request.getParameter("current");
		String name = request.getParameter("name");

		FsItemEx fsi = super.findItem(fsService, target);
		FsItemEx dst = new FsItemEx(fsi.getParent(), name);
		fsi.renameTo(dst);

		json.put("added", new Object[] { getFsItemInfo(request, dst) });
		json.put("removed", new String[] { target });
	}
}
