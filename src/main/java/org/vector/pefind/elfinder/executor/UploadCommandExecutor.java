package org.vector.pefind.elfinder.executor;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItemStream;
import org.vector.pefind.elfinder.iexecutor.CommandExecutor;
import org.vector.pefind.elfinder.iservice.FsItemFilter;
import org.vector.pefind.elfinder.iservice.FsService;
import org.vector.pefind.elfinder.model.FsItemEx;
import org.json.JSONObject;

public class UploadCommandExecutor extends AbstractJsonCommandExecutor
		implements CommandExecutor {
	
	public void execute(FsService fsService, HttpServletRequest request,
			ServletContext servletContext, JSONObject json) throws Exception {
		
		List<FileItemStream> listFiles = (List<FileItemStream>) request.getAttribute(FileItemStream.class.getName());
		List<FsItemEx> added = new ArrayList<FsItemEx>();

		String target = request.getParameter("target");
		FsItemEx dir = super.findItem(fsService, target);
		

		FsItemFilter filter = getRequestedFilter(request);
		for (FileItemStream fis : listFiles) {
			java.nio.file.Path p = java.nio.file.Paths.get(fis.getName());
			FsItemEx newFile = new FsItemEx(dir, p.getFileName().toString());
			newFile.createFile();
			InputStream is = fis.openStream();
			newFile.writeStream(is);
			if (filter.accepts(newFile))
				added.add(newFile);
		}

		json.put("added", files2JsonArray(request, added));
	}
}
