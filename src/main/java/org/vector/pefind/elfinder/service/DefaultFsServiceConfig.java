package org.vector.pefind.elfinder.service;

import org.vector.pefind.elfinder.iservice.FsServiceConfig;

public class DefaultFsServiceConfig implements FsServiceConfig
{
	private int _tmbWidth;

	public void setTmbWidth(int tmbWidth)
	{
		_tmbWidth = tmbWidth;
	}

	public int getTmbWidth()
	{
		return _tmbWidth;
	}
}
