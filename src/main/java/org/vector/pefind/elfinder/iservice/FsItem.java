package org.vector.pefind.elfinder.iservice;

public interface FsItem
{
	FsVolume getVolume();
}