package org.vector.pefind.elfinder.util;

import java.io.IOException;

import org.vector.pefind.elfinder.iservice.FsItem;
import org.vector.pefind.elfinder.iservice.FsService;
import org.vector.pefind.elfinder.model.FsItemEx;

public abstract class FsServiceUtils {
	public static FsItemEx findItem(FsService fsService, String hash)
			throws IOException {
		FsItem fsi = fsService.fromHash(hash);
		if (fsi == null) {
			return null;
		}

		return new FsItemEx(fsi, fsService);
	}
}
