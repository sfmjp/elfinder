package org.vector.pefind.elfinder.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.commons.CommonsMultipartResolver;

/**
 * 自定义文件上传Resolver
 * @author mjp
 */
public class MyCommonsMultipartResolver extends CommonsMultipartResolver {
	
	/**
	 * 重写文件上传的请求判断，如果是elfinder的文件上传请求就不由spring去处理
	 * 否则会被springmvc处理请求，导致elfinder的文件上传出错
	 */
	public boolean isMultipart(HttpServletRequest request) {
		
//		System.out.println("MyCommonsMultipartResolver.isMultipart()");
		//如果是myown的请求才走
		String uri = request.getRequestURI();
//		System.out.println("MyCommonsMultipartResolver.isMultipart() uri ==> "+uri);
		
		//elfinder的文件上传请求不能被spring去处理
		//TODO 正常的请求uri中，尤其是包含文件上传的，不能含有connector
		if(uri.contains("flconnector")){
			return false;
		}
		
		return super.isMultipart(request);
	}
}
