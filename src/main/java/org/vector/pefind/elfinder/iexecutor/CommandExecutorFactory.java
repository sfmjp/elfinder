package org.vector.pefind.elfinder.iexecutor;

public interface CommandExecutorFactory {
	CommandExecutor get(String commandName);
}