package org.vector.pefind.elfinder.iexecutor;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.vector.pefind.elfinder.iservice.FsServiceFactory;

public interface CommandExecutionContext {
	FsServiceFactory getFsServiceFactory();

	HttpServletRequest getRequest();

	HttpServletResponse getResponse();

	ServletContext getServletContext();
}
