package org.vector.pefind.elfinder.iexecutor;

public interface CommandExecutor {
	void execute(CommandExecutionContext commandExecutionContext)
			throws Exception;
}
