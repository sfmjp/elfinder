/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	//图片上传配置  
//	config.filebrowserImageUploadUrl = 'web/ckeditor/upload';//可以上传了
	config.filebrowserBrowseUrl='elfinder.html';//请求路径，如果不行试试相对路径
//	config.filebrowserImageBrowseUrl = 'upload';//
//elfinder 的配置，但是选择了图片无法获取图片地址到ckeditor编辑器中
//	config.filebrowserImageBrowseUrl = 'index.html';
	//中文简体
	config.language = 'zh-cn';
	//皮肤颜色
	config.uiColor = '#38AFF3';
};
